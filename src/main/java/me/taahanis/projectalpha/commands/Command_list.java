/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.commands;

import java.util.ArrayList;
import java.util.List;
import me.taahanis.projectalpha.ranking.Ranking.Rank;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_list implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        final List<String> names = new ArrayList<String>();
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            names.add(Rank.getRank(player).getPrefix() + " " + ChatColor.GOLD + player.getName());
        }
        sender.sendMessage( "Current players online: " + names.size() + " out of " + Bukkit.getServer().getMaxPlayers() + ":\n" + StringUtils.join(names, ChatColor.WHITE + ", "));
        
               
        return true;
       
    }
    
}
