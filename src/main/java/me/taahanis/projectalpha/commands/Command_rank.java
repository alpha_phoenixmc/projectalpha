/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.commands;

import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.ranking.Ranking.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_rank implements CommandExecutor {
    

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        if (!(sender instanceof Player))
        {
            Bukkit.getConsoleSender().sendMessage("This command only works ingame.");
            return false;
        }
        Player player = (Player) sender;
        if (!Rank.getRank(player).isAbout(Rank.DEFAULT))
        {
            player.sendMessage(ChatColor.AQUA + "I'm sorry, but how are you not atleast Rank DEFAULT or above?");
            return false;
        }
        if (args.length == 0)
        {
            player.sendMessage(ChatColor.AQUA + "You are currently a " + ChatColor.translateAlternateColorCodes('&', Rank.getRank(player).getPrefix()));
            return true;
        }
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null)
        {
            player.sendMessage(ChatColor.RED + "Player not found.");
            return false;
        }
        player.sendMessage(ChatColor.AQUA + target.getName() + " is currently a " + ChatColor.translateAlternateColorCodes('&', Rank.getRank(target).getPrefix()));
        
        return true;
    }
    
}
