/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.commands;

import me.taahanis.projectalpha.Alpha;
import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.ranking.Ranking.Rank;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_admin implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player)
        {
            sender.sendMessage(ChatColor.RED + "This is a console command, me boy.");
            return false;
        }
        if (args.length == 0)
        {
            return false;
        }
        final Player target = Bukkit.getServer().getPlayer(args[1]);
        switch (args[0])
        {
            case "add":
                if (Rank.isStaff(target))
                {
                    sender.sendMessage("Player is staff, what are you doing?");
                    return true;
                }
                Alpha.plugin.staff.addAdmin(target);
                Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Adding " + target.getName() + " to the staff list.");
                target.kickPlayer(ChatColor.GOLD + "You are now a Helper. You may rejoin.");
                
                Bukkit.getServer().reload();
                break;
            case "remove":
                if (!Rank.isStaff(target))
                {
                    sender.sendMessage("Player is not staff, meaning they were already removed.");
                    return true;
                }
                Alpha.plugin.staff.removeAdmin(target);
                Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Removing " + target.getName() + " from the staff list.");
                target.kickPlayer(ChatColor.GOLD + "You are no longer Staff. You may rejoin.");
                
                Bukkit.getServer().reload();
                break;
            case "setrank":
                if (!Rank.isStaff(target))
                {
                    sender.sendMessage("Player is not staff, try adding them first.");
                    return true;
                }
                Rank rank = Rank.findRank(args[2]);
                // /rank setrank <player> <rank>
                //        args0   args1    args2
                if (rank == null)
                {
                    sender.sendMessage("Rank not found.");
                    return true;
                }
            if (!rank.isAbout(Rank.HELPER))
            {
                 sender.sendMessage("Rank must be helper or higher.");
            }
            
            
                Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Setting " + target.getName() + "'s rank to " + rank.getName());
                Alpha.plugin.staff.setRank(target, rank.getName());
                target.kickPlayer(ChatColor.GOLD + "You are now a " + rank.getName() + "\nYou may rejoin.");
                
                Bukkit.getServer().reload();
                break;
            default:
                return false;
                
        }
        return true;
    }
    
}
