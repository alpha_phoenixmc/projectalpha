/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha;

import me.taahanis.projectalpha.ranking.Ranking;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Functions {
    
    public Alpha plugin;
    
    public Functions(Alpha instance)
    {
        plugin = instance;
    }
    
    public String getPlayerUUID(Player player)
    {
        return Alpha.plugin.cm.getPlayers().getString(player.getUniqueId().toString());
    }
    public String getPlayerIP(Player player)
    {
        return Alpha.plugin.cm.getPlayers().getString(player.getUniqueId().toString() + ".ip");
    }
    
    public String getPlayerName(Player player)
    {
        return Alpha.plugin.cm.getPlayers().getString(player.getUniqueId().toString() + ".name");
    }
    
    
}
