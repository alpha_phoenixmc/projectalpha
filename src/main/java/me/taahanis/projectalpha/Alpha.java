/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha;

import me.taahanis.projectalpha.commands.Command_admin;
import me.taahanis.projectalpha.commands.Command_clear;
import me.taahanis.projectalpha.commands.Command_list;
import me.taahanis.projectalpha.commands.Command_rank;
import me.taahanis.projectalpha.configuration.ConfigManager;
import me.taahanis.projectalpha.listener.PlayerListener;
import me.taahanis.projectalpha.ranking.Staff;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author moh_a
 */
public class Alpha extends JavaPlugin {
    
    public static Alpha plugin;
    
    public ConfigManager cm;
    public Staff staff = new Staff(this);
    public Functions func = new Functions(this);
    
    @Override
    public void onEnable()
    {
        plugin = this;
        cm = new ConfigManager();
        
        
        
        cm.setup();
        
        registerCommands();
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        
        
        getConfig().options().copyDefaults(true);
        saveConfig();
        
    }
    
    public void registerCommands()
    {
        getCommand("rank").setExecutor(new Command_rank());
        getCommand("admin").setExecutor(new Command_admin());
        getCommand("list").setExecutor(new Command_list());
        getCommand("clear").setExecutor(new Command_clear());
    }
    @Override
    public void onDisable()
    {
        saveConfig();
    }
    
}
