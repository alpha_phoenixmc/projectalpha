/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.ranking;

import me.taahanis.projectalpha.Alpha;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Staff {
    public Alpha plugin;
    public Staff(Alpha instance)
    {
     plugin = instance;   
    }
    
    public void addAdmin(Player player)
    {
        FileConfiguration staff = Alpha.plugin.cm.getStaff();
        staff.createSection(player.getUniqueId().toString());
        staff.set(player.getUniqueId().toString() + ".ip", player.getAddress().getHostString());
        staff.set(player.getUniqueId().toString() + ".rank", "Helper");
        Alpha.plugin.cm.save();
    }
    public void removeAdmin(Player player)
    {
        FileConfiguration staff = Alpha.plugin.cm.getStaff();
        staff.set(player.getUniqueId().toString(), null);
        Alpha.plugin.cm.save();
    }
    public void setRank(Player player, String rank)
    {
        FileConfiguration staff = Alpha.plugin.cm.getStaff();
        staff.set(player.getUniqueId().toString() + ".rank", rank);
        Alpha.plugin.cm.save();
    }
    
}
