/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.listener;

import me.taahanis.projectalpha.Alpha;
import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.util.AlphaLog;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author moh_a
 */
public class PlayerListener implements Listener {
    
    public Alpha plugin = Alpha.getPlugin(Alpha.class);
    
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
        Player player = event.getPlayer();
       try {
        event.setFormat(net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', Ranking.Rank.getRank(player).getPrefix())
                + " " + ChatColor.RED + 
                player.getName() + 
                ChatColor.GRAY + ": " + 
                ChatColor.WHITE + 
                event.getMessage());
                   
        
       } catch (Exception e)
       {
           AlphaLog.info("Player's format did not set. rip in the chat.");
       }
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        FileConfiguration config = plugin.cm.getPlayers();
        String pSection = plugin.cm.getPlayers().getString(player.getUniqueId().toString());
        
        if (pSection == null)
        {
            config.createSection(player.getUniqueId().toString());
            config.set(player.getUniqueId().toString() + ".ip", player.getAddress().getHostString());
            config.set(player.getUniqueId().toString() + ".name", player.getName());
            config.set(player.getUniqueId().toString() + ".isStaff", false);
            config.set(player.getUniqueId().toString() + ".punishments" + ".isbanned", false);
            config.set(player.getUniqueId().toString() + ".punishments" + ".ismuted", false);
            config.set(player.getUniqueId().toString() + ".punishments" + ".isfrozen", false);
            plugin.cm.save();
        }
        
        String taahSection = plugin.cm.getStaff().getString(player.getUniqueId().toString());
        if (player.getName().equalsIgnoreCase("PhoenixOP") && taahSection == null && !Ranking.Rank.isStaff(player))
        {
            plugin.staff.addAdmin(player);
            
        }
        if (player.getName() != plugin.func.getPlayerName(player))
        {
            config.set(player.getUniqueId().toString() + ".name", player.getName());
        }
        
    }
    
}
